**Experis Academy, Norway**

**Authors:**
* **Odd Martin Hansen**

# Task 10: Upgraded Name Search
Modify your name search solution to have the following:

A separate class that describes a person
* Reminder: It will need it’s own source file
* First Name, Last Name, Telephone, etc
* Use overloaded constructors*

Create a collection of this new class
* Modify your search to access a public method within this class


#### I did this when I initially made the name search.
[Task 5 - Name Search](https://gitlab.com/kegulf/experisnorofftask5)